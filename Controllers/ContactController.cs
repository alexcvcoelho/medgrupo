using System.Collections.Generic;
using System.Threading.Tasks;
using cadmus.Business;
using cadmus.Models;
using cadmus.Models.Base;
using Microsoft.AspNetCore.Mvc;

namespace cadmus.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContactController : ControllerBase
    {
        private readonly ContactBusiness _business;
        public ContactController(){
            _business = new ContactBusiness();
        }

       [HttpGet]
        public Task<List<Contact>> FindAll()
        {
            return _business.FindAll();
        }

        [HttpGet("{id}")]
        public Contact Find(int id)
        {
            return _business.Find(id);
        }

        [HttpPost]
        public Contact Save(Contact contact)
        {
            return _business.Save(contact);
        }

        [HttpDelete]
        public IActionResult Delete(Contact contact)
        {
            _business.Delete(contact);
            return Ok();
        }


    }
}