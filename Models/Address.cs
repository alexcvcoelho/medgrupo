using cadmus.Models.Base;

namespace cadmus.Models
{
    public class Address : BaseEntity
    {
        public string Street { get; set; }
        public int Number { get; set; }
    }
}