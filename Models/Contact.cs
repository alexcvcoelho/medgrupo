using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using cadmus.Models.Base;

namespace cadmus.Models
{
    [Table("Contact", Schema = "dbo")]
    public class Contact : BaseEntity
    {
        [Required]
        [Column(TypeName = "varchar(255)")]
        public string Name { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? BirthDate { get; set; }
        [Column(TypeName = "varchar(1)")]
        public string Gender { get; set; }
        [Column(TypeName = "int")]
        public int YearsOld { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string PhoneNumber { get; set; }
        [Column(TypeName = "varchar(20)")]
        public string CellPhoneNumber { get; set; }
    }
}