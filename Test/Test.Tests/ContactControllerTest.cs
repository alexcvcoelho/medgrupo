using System;
using cadmus.Controllers;
using cadmus.Models;
using Xunit;

namespace Test.Tests
{
    public class ContactControllerTest
    {
        private readonly ContactController _controller;
        [Fact]
        public void Create()
        {
            Contact contact = new Contact()
            {
                Name = "Alex Coelho",
                BirthDate = DateTime.Now,
                CellPhoneNumber = "342342434",
                Gender = "M",
                PhoneNumber = "53454354",
                YearsOld = 24
            };
            contact = _controller.Save(contact);
            Assert.NotEqual(contact.Id , 0);
        }
    }
}
