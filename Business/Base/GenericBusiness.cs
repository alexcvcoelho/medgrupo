using System.Collections.Generic;
using System.Threading.Tasks;
using cadmus.Business.Interface;
using cadmus.Models.Base;
using cadmus.Repository.Base;
using cadmus.Repository.Interface;

namespace cadmus.Business.Base
{
    public class GenericBusiness<T> : ICrudBusiness<T> where T: BaseEntity
    {
        protected GenericRepository<T> _repository;
        public GenericBusiness()
        {

        }
        public T Create(T model)
        {
            return _repository.Create(model);
        }

        public void Delete(T model)
        {
            _repository.Delete(model);
        }

        public T Find(int id)
        {
            return _repository.Find(id);
        }

        public Task<List<T>> FindAll()
        {
            return _repository.FindAll();
        }

        public T Save(T model)
        {
            if(model.Id == 0)
                return Create(model);
            return Update(model);
        }

        public T Update(T model)
        {
            return _repository.Update(model);
        }
    }
}