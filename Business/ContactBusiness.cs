using cadmus.Business.Base;
using cadmus.Business.Interface;
using cadmus.Models;
using cadmus.Repository;

namespace cadmus.Business
{
    public class ContactBusiness : GenericBusiness<Contact>
    {

        public ContactBusiness(){
            _repository = new ContactRepository();
        }
    }
}