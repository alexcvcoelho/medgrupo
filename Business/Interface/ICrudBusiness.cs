using System.Collections.Generic;
using System.Threading.Tasks;

namespace cadmus.Business.Interface
{
    public interface ICrudBusiness<T>
    {
        public Task<List<T>> FindAll();
        public T Find(int id);
        public T Save(T model);
        public T Create(T model);
        public T Update(T model);
        public void Delete(T model);
    }
}