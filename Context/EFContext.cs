using cadmus.Models;
using cadmus.Models.Base;
using Microsoft.EntityFrameworkCore;

namespace cadmus.Context
{
    public class EFContext : DbContext
    {
        public DbSet<Contact> Contacts { get; set; }   
  
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)  
        {  
            optionsBuilder.UseSqlServer(@"Server=localhost;DataBase=Medgrupo;Uid=sa;Pwd=Medgrupo123");  
        }   
    }
}