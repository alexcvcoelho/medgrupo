using System.Collections.Generic;
using System.Threading.Tasks;
using cadmus.Context;
using cadmus.Models.Base;
using cadmus.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace cadmus.Repository.Base
{
    public class GenericRepository<T> : ICrudRepository<T> where T: BaseEntity
    {
        protected EFContext _dbContext;
        protected DbSet<T> _context;

        public GenericRepository(){
            _dbContext = new EFContext();
        }

        public T Create(T model)
        {
            _context.Add(model);
            _dbContext.SaveChanges();
            return model;
        }

        public void Delete(T model)
        {
            _context.Remove(model);
            _dbContext.SaveChanges();
        }

        public T Find(int id)
        {
            return _context.Find(id);
        }

        public Task<List<T>> FindAll()
        {
            return _context.ToListAsync();
        }

        public T Update(T model)
        {
            _context.Update(model);
            _dbContext.SaveChanges();
            return model;
        }
    }
}