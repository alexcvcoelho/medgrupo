using cadmus.Models;
using cadmus.Repository.Base;

namespace cadmus.Repository
{
    public class ContactRepository : GenericRepository<Contact>
    {
        public ContactRepository()
        {
            _context = _dbContext.Contacts;
        }
    }
}