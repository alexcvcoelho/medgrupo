using System.Collections.Generic;
using System.Threading.Tasks;

namespace cadmus.Repository.Interface
{
    public interface ICrudRepository<T>
    {
        public Task<List<T>> FindAll();
        public T Find(int id);
        public T Create(T model);
        public T Update(T model);
        public void Delete(T model);
    }
}